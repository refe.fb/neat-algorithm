###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 27.02.2021
###  

import neat
import time
import pickle
import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import cmg_neuron
import cmg_synapse
import numpy as np
import cmg_snn
from datetime import date
import score_computing_framework as scf

animate = False
nt = neat.NEAT(100)

nt.evolve_population(3000000, 4, 3 ,0.39,"FoodGame", "Closeness", 7, animate)

#nt.evolve_population(91, 2, 2, 1., 'FoodGame', 'L1', 100, animate)


#nt.evolve_population(100, 2, 1,0.9,"XOR","L1",1,animate)

if animate:
    plt.show()
