'''
this is a suplementary code for introducing poisson based spike coding
'''
import cmg_neuron
import cmg_synapse
import matplotlib.pylab as plt
import numpy as np
import math
from matplotlib import cm

def poisson(lamb, k):
    return ( lamb**k * np.exp(-lamb)) / math.factorial(k)

n_neurons = 2

x = 0
y = 10
a = 0.02
b = 0.2
c = -65 
d = 8

neurons = []
dx = 10/ (n_neurons - 1)
for i in range(n_neurons):
    neurons.append(cmg_neuron.Neuron(a,b,c,d,x,y,'Excitatory', i ))
    neurons[-1].stdp_allowed = True
    x += dx 

x = 5
y = 5 
neuron_out = cmg_neuron.Neuron(a,b,c,d,x,y,'Excitatory', n_neurons)

for i in range(n_neurons):
    cmg_synapse.connect(neurons[i], neuron_out, 'Proximal', 5)
    
v_out = []
ws = []
last  = 0

npt = [0,1]

left_last = 0
right_last = 0

for i in range(100):
    
    strt = time.time()
    left = 5
    right = 5
    
    if npt[0] == 1:
        left = 15
    if npt[1] == 1:
        right = 15
        
    if np.random.random() < poisson(left, left_last):
        neurons[0].force()
        left_last = -1
    left_last += 1
    
    if np.random.random() < poisson(right, right_last):
        neurons[1].force()
        right_last = -1
    right_last += 1
    
    for j in range(n_neurons):
        neurons[j].tick(0)
    neuron_out.tick(0)
    
    #ws.append(neurons[0].post_synapses[0].weight)
    #v_out.append(neuron_out.v)

    
'''
plt.plot(ws)
#plt.plot(ws1)
plt.plot(v_out, label = 'v_out')
plt.legend()

plt.show()
'''