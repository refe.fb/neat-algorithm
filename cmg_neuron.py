###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 23.10.2020
###  

import numpy as np
import cmg_synapse

dt = 0.5           # Time step
threshold = 30       # Maximum membrane potential
a_ltd = 0.4        # Long term depression magnitude
a_ltp = 0.4        # Long term potentiation magnitude
t_ltd = 20           # Long term depression time constant
t_ltp = 20           # Long term potentiation time constant
t_ampa = 5           # Receptor time constant
t_nmda = 150         # Receptor time constant
t_gaba = 6           # Receptor time constant
t_gabb = 150         # Receptor time constant
speed_non = 0.15     # Non-myelinated transmission speed
speed_mye = 1.00     # Myelinated transmission speed
stdp_allowed = False     #if STDP allowed or not

class Neuron:

    def __init__(self, a, b, c, d, x, y, kind, n_id):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.x = x
        self.y = y
        self.t_depart = 0
        self.forced = 0
        self.post_synapses = []
        self.pre_synapses = []
        self.causal_fires = 0
        self.forced_fires = 0
        self.time = 0
        self.kind = kind
        self.n_id = n_id
        self.firing_now = 0
        self.all_fires = []
        self.stdp_allowed = stdp_allowed

        #the following parameters are important for inference and they change
        self.v = self.c
        self.u = self.b * self.c
        self.current = 0
        self.R = 0
        self.W = 0
        self.g_ampa = 0
        self.g_nmda = 0
        self.g_gaba = 0
        self.g_gabb = 0
        
        if (self.kind == 'Excitatory'): # Set Markram parameters
            self.U = 0.5
            self.F = 1000
            self.D = 800
        else:
            self.U = 0.2
            self.F = 20
            self.D = 700
    
    def clean_up(self,):
        self.current = 0
        self.t_depart = 0
        self.forced = 0
        self.causal_fires = 0
        self.forced_fires = 0
        self.R = 0
        self.W = 0
        self.g_ampa = 0
        self.g_nmda = 0
        self.g_gaba = 0
        self.g_gabb = 0
        self.v = self.c
        self.u = self.b * self.c
        self.firing_now = 0

    def tick(self, impulse):
        
        v = self.v         

        self.firing_now = 0
        u = self.u
        #print('post_synapses: ' + str(len(self.post_synapses)))
        self.current = self.g_ampa * v + self.g_gaba * (v + 70) + self.g_gabb * (v + 90) + \
                       self.g_nmda * ((v + 80) / 60) ** 2 / (1 + ((v + 80) / 60) ** 2) * v - impulse # Update current
        #print(self.current)

        if v >= threshold:  # Neuron fires!!!
            
            self.firing_now = 1
            self.t_depart = self.time        # Keep the time of departure
            self.v = self.c                  # Reset membrane potential
            self.u = u + self.d              # Reset inner state
            self.R += - self.R * self.W      # Update short term depression variable upon firing
            self.W += self.U * (1 - self.W)  # Update short term facilitation variable upon firing
            
            if self.forced == 0:
                self.causal_fires += 1  # Keep causal firings
            else:
                self.forced_fires += 1 # Keep forced firings
                self.forced = 0
            
            #STDP
            if self.stdp_allowed == True:
                for s_pre in self.pre_synapses:          # Apply long term potentiation/depression
                    #print(self.time, s_pre.t_arrive)
                    if  self.time - s_pre.t_arrive > 0: #potentiation
                        s_pre.weight += a_ltp * np.exp(-( self.time - s_pre.t_arrive ) / t_ltp)
                        #s_pre.weight += float(np.random.normal(0, 0.09, 1)) #random fluc.
                        #print(np.random.normal(0, 0.05, 1))
                
            self.all_fires.append(self.time)
            for s_post in self.post_synapses:
                #print( len(s_post.signals) )
                s_post.signals.append(cmg_synapse.Signal())  # Add outgoing signals to the queue
                
        else:   # Neuron is not firing
            self.firing_now = 0
            
            if self.v < -150:
                self.clean_up()
                
            self.v = v + dt * (0.04 * v ** 2 + 5 * v + 140 - u - self.current)  # Update membrane potential
            
            self.u = u + dt * (self.a * (self.b * v - u))  # Update inner state
            if self.v > threshold:  # Clip voltages higher than threshold
                self.v = threshold

        for s_post in self.post_synapses:
            if s_post.weight < 0: #not allowing any weight to go below 0
                s_post.weight = 0
            
            if s_post.weight > 100.71828: #not allowing any weight to go above e+1
                s_post.weight = 100.71828
            
            for sig in s_post.signals:

                if sig.time > s_post.delay:  # Signal reaches the post synaptic neuron
                    #print(s_post.delay)

                    #STDP
                    if self.stdp_allowed:
                        if len(s_post.post.all_fires) > 0 and (s_post.post.all_fires[-1] - self.time) < 0: #depression
                            
                            s_post.weight -= a_ltd * np.exp( ( s_post.post.all_fires[-1] - self.time ) / t_ltd)
                            #s_post.weight += float(np.random.normal(0, 0.09, 1)) #random fluc.
                            
                    s_post.t_arrive = self.time
                    if self.kind == 'Excitatory':
                        s_post.post.g_ampa += s_post.weight * self.R * self.W # Increase g_ampa of postsynaptic neuron
                        s_post.post.g_nmda += s_post.weight * self.R * self.W # Increase g_nmda of postsynaptic neuron
                    elif self.kind == 'Inhibitory':
                        s_post.post.g_gaba += s_post.weight * self.R * self.W # Increase g_gaba of postsynaptic neuron
                        s_post.post.g_gabb +=  s_post.weight * self.R * self.W # Increase g_gabb of postsynaptic neuron
                    #print(s_post.signals)
                    s_post.signals.remove(sig)  # Remove the signal from the queue
                    #print(s_post.signals)
                    
                else:
                    sig.time += dt  # Advance signals
        
        self.R += dt * (1 - self.R) / self.D        # Advance short term depression variable
        self.W += dt * (self.U - self.W) / self.F   # Advance short term facilitation variable
        self.g_ampa += - dt * self.g_ampa / t_ampa  # Decay of receptors
        self.g_nmda += - dt * self.g_nmda / t_nmda
        self.g_gaba += - dt * self.g_gaba / t_gaba
        self.g_gabb += - dt * self.g_gabb / t_gabb
        self.time += dt  # Advance time
    
    def impulse(self, magnitude):
        self.current += magnitude

    def force(self):
        self.v = threshold
        self.forced = 1

    def firing_now_val(self):
        return self.firing_now
    
    def set_stdp(self, f):
        self.stdp_allowed = f