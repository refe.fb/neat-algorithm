'''
    Authored by: Abdurrezak Efe @ Computational Modeling Group
    03.03.2021
'''
import matplotlib.cm as cm
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
plt.style.use('seaborn-white')
import cmg_neuron
import cmg_synapse
import numpy as np
import numpy 
from random import *
import random
from matplotlib.pyplot import pause
import matplotlib.pyplot as plt
import pickle
import cmg_neuron
import cmg_synapse
from pylab import *
import numpy as np
import numpy 
from random import *
import random
import pylab
from matplotlib.pyplot import pause
import pickle

class Food:

    its_a_net = False
    position = None
    prev_positions = []
    def __init__(self):

        random_x = random.randint(-100,100)
        random_y = np.sqrt(10000 - random_x**2)
        if np.random.random() > 0.5:
            random_y = random_y * -1

        self.position = [random_x, random_y, 2+2*np.random.random(), 'magenta']


        #self.position = [random.randint(-50, 750), random.randint(-750, 750), 2+2*np.random.random(), 'magenta']

class SpiNet:


    agent_id = None
    its_a_net = True
    prev_positions = []
    neurons = None
    position = None
    position = [10*np.random.random()-10*np.random.random(), 10*np.random.random()-10*np.random.random(), 4, 'blue']
    alpha_dir = 3.14159265/2
    alpha_L = 3.14159265
    alpha_R = 0
    d_eye = 3
    left_eye_proj = None
    right_eye_proj = None
    score = 0
    motor_cortex = None
    left_cor = None
    right_cor = None
    forward_cor = None
    hunger_coefficient = 0
    input_array_retina = []
    l_eye_X = None
    l_eye_Y = None
    r_eye_X = None
    ret_size = None
    r_eye_Y = None
    num_ex = None
    num_in = None
    motor_cortex_size = None
    not_seeing = 0
    with_clean_up = False
    

    def __init__(self, snn):
        
        self.neurons = snn.network_neurons()
        self.ret_size = snn.ret_size
        self.num_ex = snn.num_ex
        self.num_in = snn.num_in
        self.motor_cortex_size = snn.motor_cortex_size
        
        self.not_seeing = 0
        self.position = [25*np.random.random()-25*np.random.random(), 25*np.random.random()-25*np.random.random(), 3.5, 'blue']
        self.score = 0
        self.l_eye_X = 0 
        self.l_eye_Y = 0
        self.r_eye_X = 0
        self.r_eye_Y = 0
        self.input_array_retina = []
        for i in range(self.ret_size):
            self.input_array_retina.append(0)

        self.motor_cortex = self.neurons[snn.num_ex+snn.num_in-snn.motor_cortex_size: snn.num_ex+snn.num_in] #including pacemaker and hunger neuron
        self.left_cor = self.motor_cortex[0: int(len(self.motor_cortex)/3)]
        self.right_cor = self.motor_cortex[int(len(self.motor_cortex)/3): 2*int(len(self.motor_cortex)/3)]
        self.forward_cor = self.motor_cortex[2*int(len(self.motor_cortex)/3): len(self.motor_cortex)]

class Simulator:

    f0 = None
    axs = None
    ax0 = None
    ax1 = None
    ax2 = None
    axs = []
    consecutive_list = None
    PI = 3.14159265
    num_iter = 0
    per_iter = 0 
    simulation = False
    spinets = []
    foods = []
    num_of_foods = 0
    former = []
    food_positions = []

    def __init__(self, snns, num_iter, per_iter, simulation, num_of_foods):
        self.num_iter = num_iter
        self.full_iter = num_iter
        self.per_iter = per_iter
        self.simulation = simulation
        self.consecutive_list =[]
        for i in range(num_of_foods):
            self.foods.append(Food())

        self.num_of_foods = num_of_foods

        if self.simulation == True:

            self.f0 = plt.figure(figsize=(9, 9))
            rect = self.f0.patch
            rect.set_facecolor('gray')

            gs = self.f0.add_gridspec(16,10)
            self.ax0 = self.f0.add_subplot(111)
        
            #self.ax0 = subplot2grid((8,6), (0:4,0:2))
            self.ax0.set_facecolor('silver')
            self.ax0.axis('off')
            
    
    def f(self, x, y, agent_x, agent_y):

        ans = 0
        for i in range(len(self.food_positions)):
            ans += 1/np.sqrt((self.food_positions[i][0] - x)**2 + (self.food_positions[i][1] - y)**2)
        
        ans -= 1/np.sqrt((agent_x - x)**2 + (agent_y - y)**2)
        
        return ans 

    def set_iter(self, x):
        self.num_iter = x
    
    def Show(self, t):    
        self.ax0.clear()
        self.ax0.set_title( 'ITERATION: ' + str(t) + '\n scf fb:' +  str(self.spinets[0].score))
        
        food_position_list = []
        agent_position_list = []
        
        for i in range(len(self.spinets)):
            agent_position_list.append(self.spinets[i].prev_positions)
        
        for i in range(len(self.foods)):
            food_position_list.append(self.foods[i].prev_positions)
        
        for i in range(len(self.former)): #formerly eaten places
            self.ax0.add_patch(plt.Circle((self.former[i][0] , self.former[i][1]), 3, color='red', alpha=0.5))
            
        for i in range(self.num_of_foods):

            for position in food_position_list[i][-10:]:
                self.ax0.add_patch(position)

            self.ax0.add_patch(plt.Circle((self.foods[i].position[0] , self.foods[i].position[1]), self.foods[i].position[2], color=self.foods[i].position[3], alpha=0.5))
            self.foods[i].prev_positions.append(plt.Circle((self.foods[i].position[0] , self.foods[i].position[1]), self.foods[i].position[2]/2, color='orange', alpha=0.5))

        for i in range(len(self.spinets)):
            
            for position in agent_position_list[i][-30:]:
                self.ax0.add_patch(position)

            self.ax0.add_patch(plt.Circle((self.spinets[i].position[0] , self.spinets[i].position[1] ), self.spinets[i].position[2], color=self.spinets[i].position[3], alpha=0.5))
            self.spinets[i].prev_positions.append(plt.Circle((self.spinets[i].position[0] , self.spinets[i].position[1] ), self.spinets[i].position[2]/2, color=self.spinets[i].position[3] , alpha=0.5))
            
            self.ax0.add_patch(plt.Circle((self.spinets[i].l_eye_X , self.spinets[i].l_eye_Y), 0.7, color='orange', alpha=0.5))
            self.ax0.add_patch(plt.Circle((self.spinets[i].r_eye_X , self.spinets[i].r_eye_Y), 0.7, color='red', alpha=0.5))
            self.ax0.arrow(self.spinets[i].position[0], self.spinets[i].position[1], np.cos(self.spinets[i].alpha_dir), np.sin(self.spinets[i].alpha_dir) , head_width=0.9, head_length=100)
        
        
        self.ax0.set_aspect('equal', adjustable='datalim')
        if self.spinets[0].not_seeing == 0:
            self.f0.patch.set_facecolor("#ECFEDF")
            #self.ax0.set_facecolor("green")
        else:
            self.f0.patch.set_facecolor("#FEDFDF")
            
            #self.ax0.set_facecolor("red")
        
        #self.ax0.set_xlim(-500,500)
        #self.ax0.set_ylim(-500,500)
        
        self.ax0.plot()
        
        plt.pause(0.00001)
        
        self.ax0.cla()
        
        
        plt.cla()
        
    #takes the number of desired iterations and the number of iteraions per visualization
    def simulate_snn(self, snns, tr, scores_and_brain_ids, pr): #the network, trial number, scores and ids, process number
        #print("simulating a network" + str(snns[0]))
        
        self.spinets = []
        for i in range(len(snns)):
            self.consecutive_list.append(1) 
            self.spinets.append(SpiNet(snns[i]))
            self.spinets[i].agent_id = i
            self.spinets[i].score = 0
        
        #print("this is " + str(tr) + "th trial of " + str(scores_and_brain_ids[0][1]))
        #we clean up the previously used firings
        for i in range(len(snns)):
            for j in self.spinets[i].neurons:
                j.all_fires = []
            for k in snns[i].network_neurons():
                k.all_fires = []
        

        #for life_t in range(self.num_iter):
        life_t = 0
        while life_t < self.num_iter:
            
            #make them see for simulation
            for i in range(len(self.spinets)):
                for j in range(len(self.spinets)):
                    if i!= j:
                        self.See(self.spinets[i], self.spinets[i])
            
            for i in range(len(self.spinets)):
                for j in range(self.num_of_foods):
                    self.See(self.spinets[i], self.foods[j])
                    #print(self.spinets[i].input_array_retina)
                    if life_t == 0 and self.spinets[i].not_seeing == 1:
                        
                        self.spinets[i].not_seeing = 0
                        self.spinets[i].alpha_L += 3.14159265
                        self.spinets[i].alpha_R += 3.14159265
                        self.spinets[i].alpha_dir += 3.14159265

            self.food_positions = []
            for i in range(len(self.foods)):
                self.food_positions.append( [self.foods[i].position[0], self.foods[i].position[1]])

            if self.simulation == True and life_t % 1 == 0:
                self.Show(life_t)
                
            #for i in range(len(self.foods)):
                #self.foods[i].position[0] += np.random.random()*2-np.random.random()*2
                #self.foods[i].position[1] += np.random.random()*2-np.random.random()*2

            for i in range(len(self.spinets)):
                if life_t % 20 == 0:
                    self.Move(self.spinets[i], False)
                else:
                    self.Move(self.spinets[i], False)
            life_t += 1
        if self.simulation == True:
            #self.ax0.set_aspect('equal', adjustable='datalim')

            plt.show(block=True)

            snn = self.spinets[0]
            #here we show   the raster plot for retina, motor and all

            report_fig = plt.figure()

            plt.subplot(111)
            all_neuron_firings = []
            clrs = []
            for cell in snn.neurons:
                all_neuron_firings.append(cell.all_fires)
                if cell.n_id < snn.ret_size:
                    clrs.append('magenta')
                elif cell.kind == 'Inhibitory':
                    clrs.append('red')
                elif cell.n_id > snn.num_ex+snn.num_in: #motor 
                    clrs.append('green')
                else:
                    clrs.append('blue')

            print(len(clrs))
            print(len(all_neuron_firings))
            

            raster_plot(all_neuron_firings, clrs)
            
            plt.show()


        for i in range(len(self.spinets)):
            scores_and_brain_ids[i][0] = self.spinets[i].score
            scores_and_brain_ids[i][1] = i
        
        self.spinets = []
        return scores_and_brain_ids

    def See(self, spi_net, thing): #a spinet seeing another spinet or food
        
        f =  thing.its_a_net

        spi_net.l_eye_X = spi_net.position[0] + spi_net.d_eye*np.cos(spi_net.alpha_L)
        spi_net.l_eye_Y = spi_net.position[1] + spi_net.d_eye*np.sin(spi_net.alpha_L)
        spi_net.r_eye_X = spi_net.position[0] + spi_net.d_eye*np.cos(spi_net.alpha_R)
        spi_net.r_eye_Y = spi_net.position[1] + spi_net.d_eye*np.sin(spi_net.alpha_R)
        egim = (spi_net.r_eye_Y-spi_net.l_eye_Y)/(spi_net.r_eye_X-spi_net.l_eye_X)
        sabit = -1*(egim*spi_net.l_eye_X - spi_net.l_eye_Y)   

        l_eye_X = spi_net.l_eye_X
        l_eye_Y = spi_net.l_eye_Y
        r_eye_X = spi_net.r_eye_X
        r_eye_Y = spi_net.r_eye_Y
        position = spi_net.position
        x_h = position[0]
        y_h = position[1]
        c_h = position[3]

        alpha_dir = spi_net.alpha_dir
        alpha_L = spi_net.alpha_L
        alpha_R = spi_net.alpha_R
        d_eye = spi_net.d_eye
        left_eye_proj = spi_net.left_eye_proj
        right_eye_proj = spi_net.right_eye_proj
        input_array_retina = spi_net.input_array_retina
        
        spi_net.not_seeing = 0
        x = thing.position[0]
        y = thing.position[1]
        r = thing.position[2]
        ccc = thing.position[3]
        
        if (egim*x + sabit < y and egim*(x_h+np.cos(alpha_dir))+sabit < y_h+np.sin(alpha_dir) ) or ( egim*x + sabit > y and egim*(x_h+np.cos(alpha_dir))+sabit > y_h+np.sin(alpha_dir) ): #can see up or down )
            
            rec_val1 = r/(np.sqrt((x-l_eye_X)**2 + (y-l_eye_Y)**2)) 
            rec_val2 = r/(np.sqrt((x-r_eye_X)**2 + (y-r_eye_Y)**2))
            rec_val3 = ((-1*(x-l_eye_X)*(d_eye*np.cos(alpha_L)))+(-1*(y-l_eye_Y)*(d_eye*np.sin(alpha_L)))) / ((np.sqrt((x-l_eye_X)**2 + (y-l_eye_Y)**2))*d_eye )  
            rec_val4 =  (((x-r_eye_X)*(d_eye*np.cos(alpha_R)))+((y-r_eye_Y)*(d_eye*np.sin(alpha_R)))) / ((np.sqrt((x-r_eye_X)**2 + (y-r_eye_Y)**2))*d_eye ) 
            
            if rec_val1 < -1:
                rec_val1 = -1
            if rec_val2 < -1:
                rec_val2 = -1
            if rec_val3 < -1:
                rec_val3 = -1
            if rec_val4 < -1:
                rec_val4 = -1
            
            theta_L = np.arcsin( min(1, rec_val1 ))*180/self.PI
            theta_R = np.arcsin( min(1, rec_val2 ))*180/self.PI
            beta_L = np.arccos( min(1,  rec_val3)  )*180/self.PI
            beta_R = np.arccos( min(1, rec_val4 )  )*180/self.PI
            left_eye_proj = [int(beta_L-theta_L), int(beta_L+theta_L), ccc,int(0), int(0), c_h]
            right_eye_proj = [int(beta_R-theta_R), int(beta_R+theta_R), ccc, int(0), int(0), c_h]

        else:
            #print("cannot see at the moment")
            spi_net.not_seeing = 1
            #alpha_dir += 180
            #alpha_L += 180
            #alpha_R += 180
            left_eye_proj = [0, 0, ccc,int(0), int(0), c_h] 
            right_eye_proj = [0, 0, ccc, int(0), int(0), c_h]
        
        #print("left: " + str(left_eye_proj))
        #print("right: " + str(right_eye_proj))
        # this part determines inputs to retina neurons
        
        #print("önce:" + str(input_array_retina))
        #print("left:" + str(left_eye_proj[0]) + " " + str(left_eye_proj[1] ))
        #print("right:" + str(right_eye_proj[0]) + " " + str(right_eye_proj[1] ))
        
        for i in range(spi_net.ret_size):
            input_array_retina[i] *= 1/np.exp(2)
            
        #print("sonra:" + str(input_array_retina))
        #left_input += (min(left_eye_proj[1],90)**2 -min(left_eye_proj[0], 90)**2)
        #right_input +=  (max(right_eye_proj[1],90)**2 -max(right_eye_proj[0], 90)**2)

        oranla = 360/spi_net.ret_size
        for i in range(spi_net.ret_size):
            if i < int(spi_net.ret_size/2): #left_eye
                if left_eye_proj[1] - left_eye_proj[0] > 0: #if it is a network being seen, it goes to even numbered neurons
                    #if i%2 == 0:
                    if   i*oranla <= left_eye_proj[0] and (i+1)*oranla >= left_eye_proj[0]: #leftmost point of range
                        
                        input_array_retina[i] += 5 #(5 * ( min(left_eye_proj[1], (i+1)*oranla)  - left_eye_proj[0])  )/oranla #how much overlap
                        if ( min(left_eye_proj[1], (i+1)*oranla)  - left_eye_proj[0]) > 0:
                            input_array_retina[i] += 5
                    elif i*oranla <= left_eye_proj[1] and (i+1)*oranla >= left_eye_proj[1]: #rightmost point of range
                        
                        input_array_retina[i] += 5 #(5 * ( -max(left_eye_proj[0], (i)*oranla)  + left_eye_proj[1])  )/oranla
                        if ( -max(left_eye_proj[0], (i)*oranla)  + left_eye_proj[1]) > 0:
                            input_array_retina[i] += 5
                    elif i*oranla >= left_eye_proj[0] and (i+1)*oranla <= left_eye_proj[1]: #between ranges
                        
                        input_array_retina[i] += 10
                    
            if i >= int(spi_net.ret_size/2): #right_eye
                if  right_eye_proj[1] - right_eye_proj[0] > 0: #if it is a network being seen, it goes to even numbered neurons
                    
                    j = i-int(spi_net.ret_size/2)
                    if j*oranla <= right_eye_proj[0] and (j+1)*oranla >= right_eye_proj[0]: #leftmost point of range
                        input_array_retina[i] += 5 #(5 * ( min(right_eye_proj[1], (j+1)*oranla)  - right_eye_proj[0])  )/oranla
                        if ( min(right_eye_proj[1], (j+1)*oranla)  - right_eye_proj[0]) > 0:
                            input_array_retina[i] += 5
                    elif j*oranla <= right_eye_proj[1] and (j+1)*oranla >= right_eye_proj[1]: #rightmost point of range
                        input_array_retina[i] += 5 #(5 *  ( -max(right_eye_proj[0], (j)*oranla)  + right_eye_proj[1])  )/oranla
                        if ( -max(right_eye_proj[0], (j)*oranla)  + right_eye_proj[1]) > 0:
                            input_array_retina[i] += 5
                    elif j*oranla >= right_eye_proj[0] and (j+1)*oranla <= right_eye_proj[1]: #between ranges
                        input_array_retina[i] += 10

        #print("sonra:" + str(input_array_retina))

        spi_net.l_eye_X = l_eye_X
        spi_net.l_eye_Y = l_eye_Y
        spi_net.r_eye_X = r_eye_X
        spi_net.r_eye_Y = r_eye_Y
        spi_net.position = position
        spi_net.alpha_dir = alpha_dir
        spi_net.alpha_L = alpha_L
        spi_net.alpha_R = alpha_R
        spi_net.d_eye = d_eye
        spi_net.left_eye_proj = left_eye_proj
        spi_net.right_eye_proj = right_eye_proj
        spi_net.input_array_retina = input_array_retina

    def Move(self, spi_net, empty_ticks): 
        
        like_an_eye_lim = 21
        
        if empty_ticks: # we determine if it is for resting of the brain
            for like_an_eye in range(like_an_eye_lim):
                for i in range(len(spi_net.neurons)):
                    spi_net.neurons[i].tick(0)
        else:
            for like_an_eye in range(int(10*like_an_eye_lim/10)):
                #first retina
                for i in range(spi_net.ret_size):
                    
                    if like_an_eye < like_an_eye_lim:
                        
                        if spi_net.input_array_retina[i] > 0.5:
                            
                            spi_net.neurons[i].tick(10)
                            #if 0.25 > np.random.random(): #if input is 1, 50Hz
                             #   spi_net.neurons[i].force()
                        else:
                            spi_net.neurons[i].tick(50)
                            #if 0.05 > np.random.random(): #if no input 10Hz
                             #   spi_net.neurons[i].force()
                        
                        #spi_net.neurons[i].tick(spi_net.input_array_retina[i])
                    else:
                        spi_net.neurons[i].tick(0)
                    
                i = spi_net.ret_size
                while i < spi_net.num_ex + spi_net.num_in:
                    spi_net.neurons[i].tick(0)
                    i += 1
            
            #for like_an_eye in range(int(9*like_an_eye_lim/10)):
             #   for i in range(len(spi_net.neurons)):
              #      spi_net.neurons[i].tick(0)
        
           
        #print(spi_net.input_array_retina)
        #print("0" + str(spi_net.neurons[0].v))
        #print("2" + str(spi_net.neurons[2].v))
        
        left_go = 0
        right_go = 0
        forward_go = 0

        for i in range(int(len(spi_net.motor_cortex)/3)):
            left_go += spi_net.left_cor[i].causal_fires
            right_go += spi_net.right_cor[i].causal_fires
            forward_go += spi_net.forward_cor[i].causal_fires

        #print(left_go, right_go, forward_go)
        all_go = left_go + right_go + forward_go + 1
        left_go /= all_go
        right_go /= all_go
        forward_go /= all_go
        
        
        if(abs(right_go-left_go)> 0):
        
            #sigm_val = ( 180* (abs(right_go-left_go) ) / (like_an_eye_lim * len(spi_net.motor_cortex)/3)  )  *  (2*self.PI/360)
            sigm_val = ( 100 * (abs(right_go-left_go) )  )  *  (2*self.PI/360)
            #print(60 * (right_go-left_go))
            
            if right_go > left_go:
                spi_net.alpha_dir += sigm_val
                spi_net.alpha_L += sigm_val
                spi_net.alpha_R += sigm_val
            else:
                spi_net.alpha_dir -= sigm_val
                spi_net.alpha_L -= sigm_val
                spi_net.alpha_R -= sigm_val

        spi_net.alpha_dir = spi_net.alpha_dir%(2*self.PI)
        spi_net.alpha_L = spi_net.alpha_L%(2*self.PI)
        spi_net.alpha_R = spi_net.alpha_R%(2*self.PI)

        #here the position of the agent changes, it moves
        prev_x = spi_net.position[0]
        prev_y = spi_net.position[1]
        
        prev_pot = 0 #potential of the current position in terms of food distances

        for i in range(len(self.foods)): 
            #prev_pot += 1/np.sqrt(  (self.foods[i].position[0] - prev_x)**2 + (self.foods[i].position[1] - prev_y)**2 )
            prev_pot += np.sqrt(  (self.foods[i].position[0] - prev_x)**2 + (self.foods[i].position[1] - prev_y)**2 )

        if forward_go > 0:
            #print(forward_go)
            spi_net.position[0] += forward_go*np.cos(spi_net.alpha_dir) / 0.06 
            spi_net.position[1] += forward_go*np.sin(spi_net.alpha_dir) / 0.06
            
            spi_net.l_eye_X = spi_net.position[0] + spi_net.d_eye*np.cos(spi_net.alpha_L)
            spi_net.l_eye_Y = spi_net.position[1] + spi_net.d_eye*np.sin(spi_net.alpha_L)
            spi_net.r_eye_X = spi_net.position[0] + spi_net.d_eye*np.cos(spi_net.alpha_R)
            spi_net.r_eye_Y = spi_net.position[1] + spi_net.d_eye*np.sin(spi_net.alpha_R)

        #for i in range(len(self.foods)): 
         #   self.foods[i].position[0] += 3*(np.random.random()**2 - np.random.random()**2)
          #  self.foods[i].position[1] += 3*(np.random.random()**2 - np.random.random()**2)
            
        #calculating new potential of food
        new_pot = 0
        for i in range(len(self.foods)): 
            #new_pot += 1/np.sqrt(  (self.foods[i].position[0] - spi_net.position[0])**2 + (self.foods[i].position[1] - spi_net.position[1])**2 )
            new_pot += np.sqrt(  (self.foods[i].position[0] - spi_net.position[0])**2 + (self.foods[i].position[1] - spi_net.position[1])**2 )

        if -1 > 0: #positive
            spi_net.score += 1*self.consecutive_list[spi_net.agent_id]*(new_pot - prev_pot)
            self.consecutive_list[spi_net.agent_id] += 1
        else:
            self.consecutive_list[spi_net.agent_id] = 1
            spi_net.score += 1*(prev_pot - new_pot)
        
        for neur in spi_net.neurons:
            neur.clean_up()    

        #should be checked
        for i in range(len(spi_net.motor_cortex)):
           spi_net.motor_cortex[i].causal_fires = 0

        for i in range(len(self.foods)): #if a food gets eaten by the spinet
            
            if numpy.sqrt((self.foods[i].position[0]-spi_net.position[0])**2+(self.foods[i].position[1]-spi_net.position[1])**2) <= spi_net.position[2] + self.foods[i].position[2]:
                #print(str(i) +'th food -> : ' + str(self.foods[i].position) + ' agent -> : ' + str(spi_net.position))
                
                #self.former.append([self.foods[i].position[0], self.foods[i].position[1]])
                
                #eating provides energy so another lifetime is added
                
                if self.num_iter < 4 * self.full_iter:
                    
                    #self.num_iter += self.full_iter
                    self.set_iter(self.num_iter + self.full_iter)

                #we are choosing food positions on a circle radiused 180
                random_x = random.randint(-100,100)
                random_y = np.sqrt(10000 - random_x**2)
                if np.random.random() > 0.5:
                    random_y = random_y * -1

                self.foods[i].position[0] = random_x
                self.foods[i].position[1] = random_y
                spi_net.hunger_coefficient = -0.01
                #print('num_of_foods: ' + str(len(self.foods)))

                spi_net.score += 0 #award of eating
        
        for i in range(len(self.spinets)): #if two spinets are bouncing
            if i != spi_net.agent_id:
                if numpy.sqrt((self.spinets[i].position[0]-spi_net.position[0])**2+(self.spinets[i].position[1]-spi_net.position[1])**2) <= spi_net.position[2] + self.spinets[i].position[2]:
                    spi_net.alpha_dir += self.PI
                    spi_net.alpha_L += self.PI
                    spi_net.alpha_R += self.PI
                    spi_net.alpha_dir = spi_net.alpha_dir%(2*self.PI)
                    spi_net.alpha_L = spi_net.alpha_L%(2*self.PI)
                    spi_net.alpha_R = spi_net.alpha_R%(2*self.PI)
        
        spi_net.hunger_coefficient += 0.01 #hunger increases if we could not eat

def raster_plot(neuralData,  colors1):
    plt.eventplot(neuralData, colors = colors1)
    #plt.xlim(-1, 1900)
    # Provide the title for the spike raster plot
    plt.title(title)
    # Give x axis label for the spike raster plot
    
    # Give y axis label for the spike raster plot
    plt.ylabel('Neuron')
