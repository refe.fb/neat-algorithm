###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 03.09.2021
###  

import warnings
warnings.filterwarnings("ignore", category=UserWarning)

import copy
import pandas as pd
from matplotlib import cm
import time
import matplotlib.gridspec as gridspec
import pickle
import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import cmg_neuron
import cmg_synapse
import numpy as np
import cmg_snn
from datetime import date
import score_computing_framework as scf

class node:
    def __init__(self, x, y, id, neuron):
        self.neuron = neuron
        self.position = (x,y)
        self.x = x
        self.y = y
        self.id = id
        self.color = 'red'
        if self.neuron.kind == 'Excitatory':
            self.color = 'green'
        
class edge:
    def __init__(self, u, v, weight, disabled, innovation_number):
        self.innovation_number = innovation_number
        self.first = u
        self.second = v
        self.u = u
        self.delay = 5 + np.random.random()*15
        self.v = v
        self.weight = weight
        self.disabled = disabled
        self.color = None
        if self.disabled:
            self.color = 'red'
        else:
            self.color = "green"
    
    def set_disabled(self, dis):
        self.disabled = dis
        if dis:
            self.color = 'red'
        else:
            self.color = "green"
        
class genome:
    def __init__(self,nodes,edges):
        self.gen = [nodes, edges]
        self.species_id = -1
        self.last_score = 0
        self.inter_species_score = 0
        self.species_color = None

class NEAT:
    
    def __init__(self, pop_size):
        
        #the parameters for speciation
        self.c1 = 1
        self.c2 = 1
        self.c3 = 0.4 # we care less about the weight differences
        self.difference_treshold = 3 #we 
        
        self.scr_fa = scf.SCORER("FoodGame", "L1", 7)
        
        self.all_species = dict()
        self.species_sizes = dict()
        self.species_colors_by_species_ids = dict()
        
        self.node_list = []
        self.fig_main = plt.figure(figsize=(12,7))
        
        self.pop_score = []
        for i in range(pop_size):
            self.pop_score.append([i , 0.])
        
        self.population = []
        self.population_size = pop_size
        self.innovation_step = 0
        self.list_of_edges = dict() #keeps all the evolved edges in any species
   
    def does_edge_exist(self, eu, ev):
        
        if  tuple( [eu , ev ]) in self.list_of_edges:
            return True
        
        return False
    
    def create_random_genome(self, n_in, n_out, connection_probability ):
        
        #values for first layer
        a = 0.02
        b = 0.2
        c = -65
        d = 8
        
        x = 0
        y = 10
        
        dx = 5
        x = 5
        if n_in > 1:
            x = 0
            dx = 10/(n_in-1)
        
        nodes = []
        #here we create n_in neurons on the line y = 10
        for i in range(n_in):
            cur_node = node(x,y,i, cmg_neuron.Neuron(a, b, c, d, x, y, 'Excitatory', i))
            nodes.append(cur_node)
            self.node_list.append(i)
            x += dx
        
        x = 0
        y = 0
        
        dx = 5
        x = 5
        if n_out > 1:
            x = 0
            dx = 10/(n_out-1)
        for i in range(n_out):
            cur_node = node(x, y, i + n_in, cmg_neuron.Neuron(a, b, c, d, x, y, 'Excitatory', i + n_in))
            nodes.append(cur_node)
            self.node_list.append(i + n_in)
            x += dx
        
        edges = [] 
        
        for u in range(n_in):  
            for v in range(n_in, n_in + n_out):
                
                if np.random.random() < connection_probability:
                    
                    # we should check if this edge has been evolved or used somewhere to use
                    # the same innovation number
                    innovate_num = self.innovation_step
                    
                    if tuple([u,v]) in self.list_of_edges:
                        innovate_num = self.list_of_edges[ tuple([u,v]) ]
                    else:
                        self.innovation_step += 1
                        
                    self.list_of_edges[ tuple([u,v]) ] = innovate_num
                    
                    cur_edge = edge(u, v, np.random.random()*46.14159, False, innovate_num )
                    edges.append(cur_edge) 
        
        return genome(nodes, edges)

    def node_clearer(self, nd):
        
        a = nd.neuron.a
        b = nd.neuron.b
        c = nd.neuron.c
        d = nd.neuron.d
        x = nd.neuron.x
        y = nd.neuron.y
        idd = nd.neuron.n_id
        nrn = cmg_neuron.Neuron(a,b,c,d,x,y, nd.neuron.kind , idd)
        nw_nd = node(x,y,idd,nrn)
        
        return nw_nd

    def mutate_genome(self, gens, node_add_mutation_rate, edge_add_mutation_rate, edge_disable_mutation_rate, weight_change_mutation_rate, neuron_mutation_rate):
        
        nodes = gens.gen[0]
        edges = gens.gen[1]
    
        #node adding
        if np.random.random() < node_add_mutation_rate:

            if len(self.list_of_edges) == 0:
                pass
            else:
                
                chosen_index_of_edge = random.randint(0,len(edges)-1)
                cnt = 0
                while (edges[chosen_index_of_edge].disabled == True) and cnt <= 100:
                    cnt += 1
                    chosen_index_of_edge = random.randint(0,len(edges)-1)
                
                if cnt > 100:
                    pass
                else:
            
                    chosen_edge = edges[chosen_index_of_edge]
                    
                    '''
                    flag = True
                    
                    for nnn in nodes:
                        if chosen_edge.innovation_number == nnn.id:
                            flag = False
                    '''
                    
                    chosen_x = 0
                    chosen_y = 0
                    chosen_node_id = chosen_edge.innovation_number
                    self.node_list.append(chosen_node_id)
                    
                    for n in nodes:
                        if (n.id == chosen_edge.u) or (n.id == chosen_edge.v):
                            chosen_x += n.x
                            chosen_y += n.y
                            
                    chosen_x /= 2
                    chosen_y /= 2
                    
                    a = 0.02
                    b = 0.2
                    c = -65
                    c = -65 + 15*np.random.random() ** 2
                    d = 8
                    d = 8 - 6 * np.random.random() ** 2
                        
                    nodes.append(node (chosen_x, chosen_y, chosen_node_id, cmg_neuron.Neuron(a, b, c, d, chosen_x, chosen_y, 'Excitatory', chosen_node_id)) )
                    
                    edges[chosen_index_of_edge].disabled = True
                    edges[chosen_index_of_edge].set_disabled(True)
                    
                    if self.does_edge_exist(chosen_edge.u,chosen_node_id ) == False:
                        edges.append( edge(chosen_edge.u, chosen_node_id, 1, False, self.innovation_step ) )
                        self.list_of_edges[ tuple([chosen_edge.u, chosen_node_id]) ] = self.innovation_step
                        self.innovation_step += 1
                    else:
                        edges.append( edge(chosen_edge.u, chosen_node_id, 1, False,
                                           self.list_of_edges[ tuple([chosen_edge.u, chosen_node_id]) ] ) )
                    
                    if self.does_edge_exist(chosen_node_id, chosen_edge.v) == False:
                        edges.append( edge(chosen_node_id, chosen_edge.v, chosen_edge.weight, False, self.innovation_step ) )
                        self.list_of_edges[ tuple([chosen_node_id, chosen_edge.v]) ] = self.innovation_step
                        self.innovation_step += 1
                    else:
                        edges.append( edge( chosen_node_id, chosen_edge.v, 1, False,
                                           self.list_of_edges[ tuple([chosen_node_id, chosen_edge.v]) ] ) )
                    
                    gens = genome(nodes, edges)
            
        #edge disable or enable
        '''
        if np.random.random() < edge_disable_mutation_rate:
            
            nodes = gens.gen[0]
            edges = gens.gen[1]
            
            chosen_index_of_edge = random.randint(0,len(edges)-1)
            
            chosen_edge = edges[chosen_index_of_edge]
            
            chosen_edge.set_disabled(not(chosen_edge.disabled))
            edges[chosen_index_of_edge] = chosen_edge
            
            gens = genome(nodes, edges)
        '''
        
        #add edge
        if np.random.random() < edge_add_mutation_rate:
            # did not find an efficient algorithm that can do it in O(n) time guaranteeing to be random
            
            nodes = gens.gen[0]
            edges = gens.gen[1]
            
            possible_additions = []
            
            for n1 in nodes:
                for n2 in nodes:
                    if n1.id != n2.id:
                        if tuple([n1.id,n2.id]) not in self.list_of_edges:
                            #print(tuple([n1.id, n2.id]))
                            if n1.y > n2.y: #if this if clause is used, no recurrency can happen
                                possible_additions.append(tuple([n1.id, n2.id]))
            
            if len(possible_additions) < 1:
                pass
            else:
                chosen_new_edge_index = random.randint(0,len(possible_additions )-1)
                u = possible_additions[chosen_new_edge_index][0]
                v = possible_additions[chosen_new_edge_index][1]
                
                innovate_num = self.innovation_step
                new_edge = edge(u, v, np.random.random() * 46.14159, False, innovate_num )
                self.list_of_edges[ tuple([u, v]) ] = self.innovation_step
                self.innovation_step += 1
                edges.append(new_edge)
            
            gens = genome(nodes, edges)
            
        #change edge weight
        if np.random.random() < weight_change_mutation_rate:
            
            
            nodes = gens.gen[0]
            edges = gens.gen[1]
            
            chosen_index_of_edge = random.randint(0,len(edges)-1)
            chosen_edge = edges[chosen_index_of_edge]
            
            chosen_edge.weight = chosen_edge.weight + np.random.random() * 0.2 - np.random.random() * 0.2  
            
            edges[chosen_index_of_edge] = chosen_edge
            gens = genome(nodes, edges)
        
        #change node neuron values
        if np.random.random() < neuron_mutation_rate:
            
            nodes = gens.gen[0]
            edges = gens.gen[1]
            
            chosen_index_of_node = random.randint(0,len(nodes)-1)
            chosen_node = nodes[chosen_index_of_node]
            
            if np.random.random() < -1: #never happens
                if np.random.random() < 0.5:
                    chosen_node.neuron.kind = 'Excitatory'
                else:
                    chosen_node.neuron.kind = 'Inhibitory'
            
            if chosen_node.neuron.kind == 'Excitatory':
                a = 0.02
                b = 0.2
                c = chosen_node.neuron.c + np.random.random() * 0.01 - np.random.random() * 0.01 
                d = chosen_node.neuron.d + np.random.random() * 0.01 - np.random.random() * 0.01 
            else:
                a = chosen_node.neuron.a + np.random.random() * 0.01 - np.random.random() * 0.01 
                b = chosen_node.neuron.b + np.random.random() * 0.01 - np.random.random() * 0.01 
                c = -65
                d = 2
            
            chosen_node.neuron.a = a
            chosen_node.neuron.b = b
            chosen_node.neuron.c = c
            chosen_node.neuron.d = d
            
            chosen_node.color = 'red'
            if chosen_node.neuron.kind == 'Excitatory':
                chosen_node.color = 'green'
            
            nodes[chosen_index_of_node] = chosen_node
            
            gens = genome(nodes, edges)
            
        return  gens
    
    def initialize_population(self, n_in, n_out, connect_prob): #this is first generation creater
        
        for i in range(self.population_size):
            gnm = self.create_random_genome(n_in, n_out, connect_prob)
            self.population.append(gnm)
    
    def difference_of_genomes(self, genome1, genome2):
        
        edges1 = genome1.gen[1]
        edges2 = genome2.gen[1]
        
        edges1.sort(key = lambda x : x.innovation_number, reverse = False)
        edges2.sort(key = lambda x : x.innovation_number, reverse = False)
        
        D = 0
        E = 0
        dW = 0
        common = 1
        N = 0
        
        j = 0
        i = 0
        while(i < len(edges1) and j < len(edges2) ):
            
            if edges1[i].innovation_number <= edges2[j].innovation_number:
                
                if edges1[i].innovation_number == edges2[j].innovation_number: # the same gene, so wee look for weight difference
                    common += 1
                    dW += ( (edges1[i].weight - edges2[j].weight) / 46.14159)  **2
                    i += 1
                    j += 1
                else: #it means its a disjoint gene
                    i += 1
                    D += 1
            else:
                j += 1
                D += 1
            
        E += len(edges1) - i
        E += len(edges2) - j    
        N = E + D + common
        
        delta = self.c1 * (E/N) + self.c2 * (D/N) + self.c3 * ( np.sqrt(dW) / common )
        
        #print("diff_weight: ", self.c1*(E/N) , self.c2*(D/N) , self.c3*( np.sqrt( dW ) / common), delta)
        return delta 
        
    def speciate_population(self,): 
        
        #start with the first genome
        self.all_species = dict()
        
        #we will speciate all of the genomes in population
        print("pop_sizeeeeeeeeeeee:", len(self.population))
        
        for i in range(len(self.population)):
            
            if self.population[i].species_id == -1: #it has no assigned species
                
                self.population[i].species_id = random.randint(0, 999999999999999999)
                
                
            for j in range(len(self.population)):
                
                if i != j and self.population[j].species_id == -1:
                    
                    if self.difference_of_genomes(self.population[j], self.population[i]) <= self.difference_treshold:
                        
                        self.population[j].species_id = self.population[i].species_id
            
            self.population[i].species_color = cm.nipy_spectral(self.population[i].species_id / 999999999999999999 ) 
            
        #we use a hashmap to keep lists of each current species    
        for gnm in self.population:
            
            if gnm.species_id not in self.all_species:
                self.all_species[ gnm.species_id ] = []
        
        for gnm in self.population:
            self.all_species[ gnm.species_id ].append(gnm)
            self.species_colors_by_species_ids[gnm.species_id] = gnm.species_color
        
    def score_snn(self, snn, problem_type, loss_func, num_trial, n_in, n_out): #this function scores a spiking neural network
        
        if problem_type == "FoodGame":
            score =  self.scr_fa.evaluate(snn, n_in, n_out)
        
        elif problem_type == "XOR":
            scorer_instance = scf.SCORER("XOR", "L1", 301)
            score = scorer_instance.evaluate(snn, 2, 1)

        return score 
        
    def genome_to_snn(self, gnm): #this function return a neural network from genome
        
        snn = cmg_snn.SNN()
        snn.initialize_snn(gnm)
        #snn.show_snn()
        return snn

    def genome_to_string(self, genome):
        
        nodes = genome.gen[0]
        edges = genome.gen[1]
        
        node_string = "{"
        for n in nodes:
            node_string += "[x: " + str(n.x) + " y: " + str(n.y) + " id: " + str(n.id) + "], "
        node_string += "}"
        
        edge_string = "{"
        for e in edges:
            edge_string += "[u: " + str(e.u) + " v: " + str(e.v) + " in_num: " + str(e.innovation_number) + " dis: " +str(e.disabled) + " w: " +str(e.weight) + " ], "  
        edge_string += "}"
        
        print("*****")
        print("nodes: " + node_string)
        print("edges: " + edge_string)

    def nodes_merger(self, nodes1, nodes2, mutation_rate): #this function merges two lists of nodes
        
        #merging nodes firstly, guaranteed that nodes are in sorted order of id
        nodes_new = []
        i = 0
        j = 0
        
        nodes1.sort(key=lambda x: x.id, reverse=False)
        nodes2.sort(key=lambda x: x.id, reverse=False)
        
        while(i < len(nodes1) and j < len(nodes2)):
            
            if nodes1[i].id <= nodes2[j].id:
                nodes_new.append( node(nodes1[i].x, nodes1[i].y, nodes1[i].id, nodes1[i].neuron ) )
                i += 1
                if nodes1[i-1].id == nodes2[j].id:
                    j += 1
            else:
                nodes_new.append( node(nodes2[j].x, nodes2[j].y, nodes2[j].id, nodes2[j].neuron ) )
                j += 1
        
        while(i < len(nodes1)):
            nodes_new.append( node(nodes1[i].x, nodes1[i].y, nodes1[i].id, nodes1[i].neuron ) )
            i += 1
        
        while(j < len(nodes2)):
            nodes_new.append( node(nodes2[j].x, nodes2[j].y, nodes2[j].id, nodes2[j].neuron ) )
            j += 1
        
        return nodes_new

    def edges_merger(self, edges1, edges2, mutation_rate): # this function merges two lists of edges
        
        edges1.sort(key=lambda x: x.innovation_number, reverse=False)
        edges2.sort(key=lambda x: x.innovation_number, reverse=False)
        
        edges_new = []
        i = 0
        j = 0
        while( i < len(edges1) and j < len(edges2)):
        
            if edges1[i].innovation_number <= edges2[j].innovation_number:
                
                if edges1[i].innovation_number == edges2[j].innovation_number:
                    #the following command is so important that when we mate two agents if we have heterogeny
                    # in terms of being disabled, the next generation may have nodes where there cannot be
                    
                    d = (edges1[i].disabled or edges2[j].disabled)
                    
                    if np.random.random() < 0.5: # if they have a pair, we could choose either one
                        edges_new.append( edge(edges2[j].u, edges2[j].v, edges2[j].weight, d, edges2[j].innovation_number ) )
                    else:
                        edges_new.append( edge(edges1[i].u, edges1[i].v, edges1[i].weight, d, edges1[i].innovation_number ) )
                    i += 1
                    j += 1
                
                else:
                    w = edges1[i].weight
                    d = edges1[i].disabled 
                
                    edges_new.append( edge(edges1[i].u, edges1[i].v, w, d, edges1[i].innovation_number ) )
                    i += 1
                
            else:
                w = edges2[j].weight
                d = edges2[j].disabled
                
                edges_new.append( edge(edges2[j].u, edges2[j].v, w, d, edges2[j].innovation_number ) )
                j += 1
            
        while( i < len(edges1) ):
            
            w = edges1[i].weight
            d = edges1[i].disabled
            
            edges_new.append( edge(edges1[i].u, edges1[i].v, w, d, edges1[i].innovation_number ) )
            i += 1
        
        while( j < len(edges2) ):
            w = edges2[j].weight
            d = edges2[j].disabled
            
            edges_new.append( edge(edges2[j].u, edges2[j].v, w, d, edges2[j].innovation_number ) )
            j += 1
        
        return edges_new

    def mate_two_genomes(self, genome1, genome2 ,mutation_rate): #this funciton mates two parents
        
        nodes1 = genome1.gen[0]
        nodes2 = genome2.gen[0]
        edges1 = genome1.gen[1]
        edges2 = genome2.gen[1]

        nodes_new = self.nodes_merger(nodes1, nodes2, mutation_rate)
        edges_new = self.edges_merger(edges1, edges2, mutation_rate)
        
        genome_all = genome(nodes_new, edges_new)
        #added mutation        
        genome_all = self.mutate_genome(genome_all, 0.1251, 0.1151, 0.0151, 0.0751, 0.00151)
        return genome_all
        
    def show_mating_result(self, genome1, genome2, ): # if you want to visualize two agents mating
        
        fig = plt.figure()
        
        G1 = self.generate_graph_from_genome(genome1)
        G2 = self.generate_graph_from_genome(genome2)
        G3 = self.generate_graph_from_genome( self.mate_two_genomes(genome1, genome2, 0) )
        
        pos1 = nx.get_node_attributes(G1,'pos')
        edges1 = G1.edges()
        colors1 = [G1[u][v]['color'] for u,v in edges1]
        weights1 = [G1[u][v]['weight'] for u,v in edges1]
        
        pos2 = nx.get_node_attributes(G2,'pos')
        edges2 = G2.edges()
        colors2 = [G2[u][v]['color'] for u,v in edges2]
        weights2 = [G2[u][v]['weight'] for u,v in edges2]
        
        pos3 = nx.get_node_attributes(G3,'pos')
        edges3 = G3.edges()
        colors3 = [G3[u][v]['color'] for u,v in edges3]
        weights3 = [G3[u][v]['weight'] for u,v in edges3]
        
        ax1 = fig.add_subplot(231) # first parent
        nx.draw(G1, pos1, edges=edges1, node_size = 7, edge_color=colors1, width=np.array(weights1)/7.7) 
        
        ax2 = fig.add_subplot(233) # second parent
        nx.draw(G2, pos2, edges=edges2, node_size = 7,edge_color=colors2, width=np.array(weights2)/7.7) 
        
        ax3 = fig.add_subplot(235) # child
        nx.draw(G3, pos3, edges=edges3, node_size = 7,edge_color=colors3, width=np.array(weights3)/7.7) 
        plt.show()
    
    def graph_visualize(self, gnm_lst, tit, avg_scrs, bst_scores, bst_score):
        
        print(1)
        fig = self.fig_main
        
        spec = gridspec.GridSpec(ncols = 5, nrows = 3, figure=fig)
        
        plt.clf()
        
        fig.suptitle(str(tit))
        print(2)
        for i in range(int(len(gnm_lst) / 15) ):
            
            G = self.generate_graph_from_genome(gnm_lst[i])
            
            pos1 = nx.get_node_attributes(G,'pos')
            edges1 = G.edges()
            colors1 = [G[u][v]['color'] for u,v in edges1]
            weights1 = [G[u][v]['weight'] for u,v in edges1]
            colors2 = nx.get_node_attributes(G,'color')
            
            layout = dict((n, G.nodes[n]["pos"]) for n in G.nodes())
            
            xx = int(i%5)
            yy = int(i/5)
            
            ax1 = fig.add_subplot(spec[yy, xx])
            ax1.cla()
            fig.set_facecolor("red")
            
            ax1.set_xlim(-12,25)
            ax1.set_ylim(-13,25)
            
            species_color_now = self.population[i].species_color
            
            nx.draw(G, pos1, node_size = 27,edge_color=colors1,node_color = np.array([species_color_now]), with_labels=True, font_size = 4, width=np.array(weights1)/100  ) # node_color = list(colors2.values())) 
            
            labels = nx.get_edge_attributes(G,'label')
            nx.draw_networkx_edge_labels(G,pos1,edge_labels=labels, font_size =  3)
        
        print(3)   
        #plt.pause(5)
        ax1 = fig.add_subplot(spec[2:, 0:2])
        ax1.cla()
        #ax1 = plt.subplot2grid((15,15), (10,0), colspan=15, rowspan = 5)
        ax1.set_title(str(avg_scrs[-1]), fontsize = 5)
        
        ax1.plot(avg_scrs, label = "average_score")
        ax1.plot(bst_score, label = "best_score")
        ax1.plot(bst_scores, label= "best_score_of_generation")
        ax1.legend()
        #print("edge_list: ", self.list_of_edges)
        
        ax2 = fig.add_subplot(spec[2:, 3:])
        ax2.cla()
        #ax1 = plt.subplot2grid((15,15), (10,0), colspan=15, rowspan = 5)
        ax2.set_title("species evolution", fontsize = 5)
        
        #keeping a temporary dictionary to plot
        temp_dict = copy.deepcopy(self.species_sizes)
        
        prev_k = -1
        print(4)
        for i, (k, v) in enumerate(temp_dict.items()):
            
            if i > 0:
                
                for j in range(len(temp_dict[k])):
                    
                     temp_dict[k][j] += temp_dict[prev_k][j]
            
            prev_k = k
            
        for kk in temp_dict:
            clr = self.species_colors_by_species_ids[kk]
            ax2.plot(list( temp_dict[kk] ), color = clr ,label = str( ( self.species_sizes[kk][-1] ) ))
            
        #ax2.legend()
        
        if len(avg_scrs) % 1 == 0:
            pd.DataFrame({'average_scores ' : np.array(avg_scrs), 
                        'best_score' : np.array(bst_score), 
                        'best_scores_of_gens': np.array(bst_scores) }).to_csv("some_data.csv")
        
        plt.pause(0.01)
        
    def generate_graph_from_genome(self, genome): # this function creates networkx graph of an agent genome

        nodes = genome.gen[0]
        edges = genome.gen[1]
        G = nx.DiGraph()    
        
        for n in nodes:    
            G.add_node(n.id , pos = n.position , color = n.color)
        
        for e in edges:
            G.add_edge(e.first, e.second, weight=e.weight, color= e.color, label = e.innovation_number)
        
        clrs =nx.get_node_attributes(G,'color')
        
        return G

    def save_genome_as_snn(self, gnm, scr):
        
        agents_score = scr
        agnt = self.genome_to_snn(gnm)
        today = date.today()
        sss = "bst_snns/best_snn_at_" + str(today) + "__" + str(agents_score)
        file_best_baby = open(sss, 'wb') 
        pickle.dump(agnt, file_best_baby)
    
    def evolve_population(self, acceptance_score, n_in, n_out, connection_probability, problem_type, loss_func, num_trial, animate):
        
        self.innovation_step = n_in + n_out
        self.initialize_population( n_in, n_out, connection_probability )
        gen_num = 0
        best_score = 0
        avg_scores = []
        bst_scores = []
        bst_score = []
        
        while best_score < acceptance_score:
            
            strt = time.time()
            print("****************************************")
            
            print("*** This is: ", gen_num, " th generation ")
            gen_num += 1
            print("best_score: ", best_score)
            scr = 0
            
            for i in range(len(self.population)):
                
                gnm = self.population[i]
                agent = self.genome_to_snn(gnm)
                
                self.pop_score[i][1] =  self.score_snn(agent, problem_type, loss_func, num_trial, n_in, n_out)
                self.population[i].last_score = self.pop_score[i][1]
                scr += self.pop_score[i][1]
            
            print(time.time() - strt,  "Scoring timee ")    
                
            for cnt in range(self.population_size):
                for cnt2 in range(len(self.population[cnt].gen[0])):
                    self.population[cnt].gen[0][cnt2] = self.node_clearer(self.population[cnt].gen[0][cnt2])

            #determined the species id of each genome
            self.speciate_population()
            
            for sp in self.all_species:
                if sp not in self.species_sizes:
                    self.species_sizes[sp] = []
                    for i in range(gen_num):
                        self.species_sizes[sp].append(0)
                
                self.species_sizes[sp].append(len(self.all_species[sp]))
                    
            extinct_species_ids = []
            for sp in self.species_sizes:
                
                if len( self.species_sizes[sp] ) < gen_num + 1:
                    
                    extinct_species_ids.append(sp)
            
            for esi in extinct_species_ids:
                
                del self.species_sizes[esi]
            
            print("number of species: ", len(self.all_species))
            
            # *** updating scores ***
            # note that we do not need to update the scores of the genomes in
            # the hashmap as they cannot necessarily affect as they are going to be divided 
            # with the same factor( namely scpecies size )
            
            for i in range( len( self.population ) ):
                
                cur_gnm = self.population[i]
                #print("ps_bfr:" , self.pop_score[i][1])
                self.pop_score[i][1] /= len(self.all_species[cur_gnm.species_id])
                #print("ps_aftr:" , self.pop_score[i][1])
                self.population[i].inter_species_score = self.pop_score[i][1]
            
            # we should choose a treshold to move on to mating top 50 here
            self.pop_score.sort(key = lambda x: x[1], reverse = True)
            #treshhold_score = self.pop_score[49][1]
            
            sum_of_scores_for_alpha = 0.
            
            for key in self.all_species:
                
                sum_of_scores_for_alpha += sum(ag.last_score for ag in self.all_species[key]) / len(self.all_species[key])
                
            #print("soal:", sum_of_scores_for_alpha)
            alpha = self.population_size / (sum_of_scores_for_alpha)
            
            
            print("alpha:", alpha)
            #create a new population
            new_pop = []
            for key in self.all_species:
                
                if len(self.all_species[key]) >= 5:
                    
                    self.all_species[key].sort(key = lambda x : x.last_score, reverse = True )
                    
                    # send the best to the next generation directly if pop.size >= 5
                    new_pop.append(self.all_species[key][0])
        
            #we are not done creating next generation
            for key in self.all_species:
                
                #we calculate the number of allowed offsprings for this species the following way
                off_springs_allowed = alpha * ( sum(ag.last_score for ag in self.all_species[key]) / len(self.all_species[key]) )
                print("len:",len(self.all_species[key])   ," osal: ",off_springs_allowed)
                
                if len(self.all_species[key]) > 1:
                    
                    self.all_species[key].sort(key = lambda x : x.last_score, reverse = True )

                    count_of_ags = 0
                    for i in range( len(self.all_species[key]) ):
                        
                        for j in range( i+1, len(self.all_species[key]) ):
                                    
                            if count_of_ags < off_springs_allowed: 
                                
                                count_of_ags += 1
                                new_pop.append(self.mate_two_genomes(self.all_species[key][i], self.all_species[key][j], 0.))
            
            
            if len(new_pop) < self.population_size:
                print(" npp: ", len(new_pop))
                
                for i in range(len(self.population)):
                    
                    for j in range(i+1, len(self.population)):
                        
                        #print("should be very rare")
                        if len(new_pop) == self.population_size:
                            break
                        
                        new_pop.append( self.mate_two_genomes(self.population[self.pop_score[i][0]], self.population[self.pop_score[j][0]], 0.) )
            
            if len(new_pop) >= self.population_size:
                #print("here here:", len(new_pop) )
                #random.shuffle(new_pop[11:])
                new_pop = new_pop[0:self.population_size]  
            
            self.population.sort(key=lambda x: x.last_score, reverse = True)
            
            bst_scores.append(self.population[0].last_score)
            bst_of_gen = self.population[0].last_score
            
            self.save_genome_as_snn(self.population[0], 3.14159) # this is the best baby of last gen, always
            
            print("best_score_gen:", bst_of_gen)
            
            if bst_of_gen > best_score:
                best_score = bst_of_gen
                self.save_genome_as_snn(self.population[0], self.population[0].last_score) 
                
            bst_score.append(best_score)
            
            scr /= len(self.population)
            avg_scores.append(scr)
            
            
            
            if animate and gen_num % 10 == 0:
                self.graph_visualize(self.population, gen_num, avg_scores, bst_scores, bst_score)
                
            self.population = new_pop
            print("Time taken:", time.time() - strt)
        
        

