###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 18.02.2021
###  

import warnings
warnings.filterwarnings("ignore", category=UserWarning)

import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import cmg_neuron
import time
import cmg_synapse
import numpy as np
import cmg_snn
import cmg_network_simulator

class SpiNet:

    agent_id = None
    its_a_net = True
    prev_positions = []
    neurons = None
    position = None
    position = [10*np.random.random()-10*np.random.random(), 10*np.random.random()-10*np.random.random(), 4, 'blue']
    alpha_dir = 3.14159265/2
    alpha_L = 3.14159265
    alpha_R = 0
    d_eye = 3
    left_eye_proj = None
    right_eye_proj = None
    score = 0
    motor_cortex = None
    left_cor = None
    right_cor = None
    forward_cor = None
    hunger_coefficient = 0
    input_array_retina = []
    l_eye_X = None
    l_eye_Y = None
    r_eye_X = None
    ret_size = None
    r_eye_Y = None
    num_ex = None
    num_in = None
    motor_cortex_size = None
    not_seeing = 0
    
    def network_neurons(self):
        
        return self.neurons

    def __init__(self, neurons, num_ex, num_in, n_in, n_out):
        self.agent_id = None
        self.its_a_net = True
        self.prev_positions = []
        self.neurons = None
        self.position = None
        self.position = [10*np.random.random()-10*np.random.random(), 10*np.random.random()-10*np.random.random(), 4, 'blue']
        self.alpha_dir = 3.14159265/2
        self.alpha_L = 3.14159265
        self.alpha_R = 0
        self.d_eye = 3
        self.left_eye_proj = None
        self.right_eye_proj = None
        self.score = 0
        self.motor_cortex = None
        self.left_cor = None
        self.right_cor = None
        self.forward_cor = None
        self.hunger_coefficient = 0
        self.input_array_retina = []
        self.l_eye_X = None
        self.l_eye_Y = None
        self.r_eye_X = None
        self.ret_size = None
        self.r_eye_Y = None
        self.num_ex = None
        self.num_in = None
        self.motor_cortex_size = None
        self.not_seeing = 0
        self.neurons = neurons
        self.ret_size = n_in
        self.num_ex = num_ex
        self.num_in = num_in
        self.motor_cortex_size = n_out
        self.not_seeing = 0
        self.position = [25*np.random.random()-25*np.random.random(), 25*np.random.random()-25*np.random.random(), 3.5, 'blue']
        self.score = 0
        self.l_eye_X = 0 
        self.l_eye_Y = 0
        self.r_eye_X = 0
        self.r_eye_Y = 0
        self.input_array_retina = []
        for i in range(self.ret_size):
            self.input_array_retina.append(0)

        self.motor_cortex = self.neurons[num_ex+num_in-n_out: num_ex+num_in] #including pacemaker and hunger neuron
        self.left_cor = self.motor_cortex[0: int(len(self.motor_cortex)/3)]
        self.right_cor = self.motor_cortex[int(len(self.motor_cortex)/3): 2*int(len(self.motor_cortex)/3)]
        self.forward_cor = self.motor_cortex[2*int(len(self.motor_cortex)/3): len(self.motor_cortex)]    
    
class SCORER:
    
    PI = 3.14159
    
    def __init__(self, pr_type, loss_func, trial):
        
        self.loss_function = loss_func
        self.problem = pr_type
        self.trial = trial
        self.scorer = cmg_network_simulator.Simulator(None, 150, 1, False, 1)
        self.PI = 3.14159
        
    def evaluate(self, snn, n_in, n_out):
        
        if self.problem == 'XOR':
            
            # n_in, n_out are conceptually known already
            # it is 2 and 2 
            # if the input is 1 0 the output neuron should fire at least once
            # if the input is 0 1 the output neuron should fire at least once
            # if the input is 0 0 the output neuron should fire at none
            # if the input is 1 1 the output neuron should fire at none
            # neuron with id = 2 represents output
            
            correct_1 = 0.
            correct_0 = 0.
            incorrect_1 = 0.
            incorrect_0 = 0.
            
            for t in range(self.trial):
                
                #input
                npt = [0 , 0] 
                npt[0] = random.randint(0, 1)
                npt[1] = random.randint(0, 1)
                
                for repeat in range(101):
                    
                    if npt[0] == 1:
                        snn.neurons[0].tick(50) # 1 is 10
                    else:
                        snn.neurons[0].tick(10) # 0 is 5
                    
                    if npt[1] == 1:
                        snn.neurons[1].tick(50)
                    else:
                        snn.neurons[1].tick(10)
                    
                    for i in range(2,len(snn.neurons)):
                        snn.neurons[i].tick(0)
                
                number_of_causal_firings0 = snn.neurons[2].causal_fires
                #number_of_causal_firings1 = snn.neurons[3].causal_fires
                #print(number_of_causal_firings0/10)
                #print(npt, number_of_causal_firings)
                
                for nnn in snn.neurons:
                    nnn.clean_up()
                    
                #print(number_of_causal_firings0)
                if npt[0] != npt[1]:
                    
                    if number_of_causal_firings0 > 5:
                    
                    #if number_of_causal_firings1 > number_of_causal_firings0: #we call 1 a 1
                        correct_1 += 1
                    else:
                        incorrect_1 += 1 #we call 1 a 0
                
                if npt[0] == npt[1]:
                    if number_of_causal_firings0 <= 5:
                    #if number_of_causal_firings1 < number_of_causal_firings0:
                        correct_1 += 1
                    else:
                        incorrect_1 += 1
                
            acc_1 = correct_1 / (correct_1 + incorrect_1)
            #acc_0 = correct_0 / (correct_0 + incorrect_0)
            
            return acc_1 #* acc_0
            
        if self.problem == 'Pole':
            pass
            #TODO

        if self.problem == "FoodGame":
            
            spi_net = self.snn2spi_net(snn, n_in, n_out )
            
            trial = self.trial #number of trials
            sc_and_br_ids = [[0,0]]
            
            for tr in range(trial):
                
                #if tr % 10 == 0:
                 #   print(str(tr)+ "th trial" )
                
                scores_and_brain_ids = [[0,0]]

                self.scorer.set_iter(150)
                #print(sc_and_br_ids[0][0])
                
                sc_and_br_ids[0][0] += (self.scorer.simulate_snn([ spi_net ], tr, [scores_and_brain_ids[0]], 1)[0][0] / trial )
                #print(sc_and_br_ids[0][0], "***")
                
            return sc_and_br_ids[0][0]
            
    def snn2spi_net(self, snn, n_in, n_out):
        
        spi_net = SpiNet(snn.neurons, snn.get_number_of_excitatory(), len(snn.neurons)-snn.get_number_of_excitatory(), n_in, n_out )
        
        return spi_net
        
        
        
              
            
            
            
            
            