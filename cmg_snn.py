###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 20.01.2021
###  

import matplotlib.pyplot as plt
import cmg_neuron
import cmg_synapse
import numpy as np
import numpy 
from random import *
from matplotlib import cm
import random

class SNN:
    
    def __init__(self, ):
        
        self.neurons = []
        self.genome = None
        
    def initialize_snn(self, gnm): #it takes a genome and initializes an SNN 
        
        nodes = gnm.gen[0]
        edges = gnm.gen[1]
        
        for n in nodes: 
            self.neurons.append(n.neuron)
        
        for e in edges:
            
            u = e.u
            v = e.v
            w = e.weight
            delay_here = e.delay
            
            for node1 in nodes:
                if node1.id == u:
                    for node2 in nodes:
                        if node2.id == v:
                            if not(e.disabled):
                                cmg_synapse.connect(node1.neuron, node2.neuron, 'Proximal', w , delay_here)
                                
            
    def show_snn(self,): #see the snn version
        
        fig = plt.figure()
        ax = fig.add_subplot(111)
        
        for n in self.neurons:
            
            ax.add_patch(plt.Circle((n.x, n.y), (n.v + 200) / 1500, color="magenta"))
        
            for s in n.post_synapses:
                ax.arrow(n.x, n.y, s.post.x- n.x, s.post.y - n.y, linewidth=0.3,head_width=0.1, head_length=0.2, fc=cm.Purples(s.weight / 1.9), ec='k')
        ax.set_xlim(-1,11)
        ax.set_ylim(-1,11)
        
        plt.show()
        
    def get_number_of_excitatory(self,):
        
        ans = 0
        for n in self.neurons:
            
            if n.kind == 'Excitatory':
                
                ans += 1
        
        return ans