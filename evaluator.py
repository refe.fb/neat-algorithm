###
#   this is an implementation of NEAT algorithm eval 
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 28.02.2020
###  

import time
import pickle
import random
import matplotlib as mpl
import matplotlib.pylab as plt
import cmg_network_simulator
import matplotlib.pyplot as plt
import networkx as nx
import cmg_neuron
import cmg_synapse
import numpy as np
import cmg_snn
from datetime import date
import score_computing_framework as scf

def run_agent_XOR(snn):
    
    v0 = []
    v1 = []
    v2 = []
    
    correct_1 = 0.
    correct_0 = 0.
    incorrect_1 = 0.
    incorrect_0 = 0.
    
    for trial in range(10):
        
        x = random.randint(0,2)
        y = random.randint(0,2)
        
        npt = [x , y] 
        
        for repeat in range(101):
            
            if npt[0] == 1:
                snn.neurons[0].tick(50)
            else:
                snn.neurons[0].tick(10)
                
            if npt[1] == 1:
                snn.neurons[1].tick(50)
            else:
                snn.neurons[1].tick(10)
            
            for i in range(2,len(snn.neurons)):
                snn.neurons[i].tick(0)
            
            v0.append(snn.neurons[0].v)
            v1.append(snn.neurons[1].v)
            v2.append(snn.neurons[2].v)
        
        for i in range(0,len(snn.neurons)):
                snn.neurons[i].clean_up()
        
        
        for repeat in range(201):
            for i in range(0,len(snn.neurons)):
                snn.neurons[i].tick(0)
            v0.append(snn.neurons[0].v)
            v1.append(snn.neurons[1].v)
            v2.append(snn.neurons[2].v)
        
        number_of_causal_firings0 = snn.neurons[2].causal_fires
        
        if npt[0] != npt[1]:
                    
            if number_of_causal_firings0 > 5:
            
            #if number_of_causal_firings1 > number_of_causal_firings0: #we call 1 a 1
                correct_1 += 1
            else:
                incorrect_1 += 1 #we call 1 a 0
        
        if npt[0] == npt[1]:
            if number_of_causal_firings0 <= 5:
            #if number_of_causal_firings1 < number_of_causal_firings0:
                correct_1 += 1
            else:
                incorrect_1 += 1
    
    acc_1 = correct_1 / (correct_1 + incorrect_1)
    print(acc_1)

    
    snn.show_snn()
    fig, axs = plt.subplots(3)
    axs[0].plot(v0, label = "v0")
    axs[1].plot(v1, label = "v1")
    axs[2].plot(v2, label = "v2")
    axs[0].legend()
    axs[1].legend()
    axs[2].legend()
    
    plt.show()
    
def run_agent_FoodGame(snn, life_time = 150):
    
    ans = cmg_network_simulator.Simulator(None, life_time, 1, True, 1)
    scf_instance = scf.SCORER("FoodGame", "L1", 1)
    
    snn = scf_instance.snn2spi_net(snn, 4, 3)

    a_list = []
    pr = 1
    l = [snn]

    ans.simulate_snn(l, 1, a_list, 1)
    
fileObject = open('bst_snns/best_snn_at_2021-03-03__2067.746706760873', 'rb')
agent = pickle.load(fileObject)

run_agent_FoodGame(agent, 150)
#run_agent_XOR(agent)




