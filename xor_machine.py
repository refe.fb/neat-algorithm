
import warnings
warnings.filterwarnings("ignore", category=UserWarning)

import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import networkx as nx
import cmg_neuron
import cmg_synapse
import numpy as np
import cmg_snn

from matplotlib import cm

neurons = []
vols = []

a = 0.02
b = 0.2
c = -68
d = 8
x = 0
y = 10
for i in range(6):
    neurons.append(cmg_neuron.Neuron(a,b,c,d,x,y,"Excitatory", i))
    vols.append([])
    
neurons[1].x = 10
neurons[2].x = 5
neurons[2].y = 0
neurons[3].x = 2.5
neurons[3].y = 5
neurons[5].x = 7.5
neurons[5].y = 5
neurons[4].x = 5
neurons[4].y = 5
#neurons[4].kind = 'Inhibitory'
#neurons[4].b = 0.25
#neurons[4].d = 2

cmg_synapse.connect(neurons[0], neurons[3], 'Proximal', 5)
cmg_synapse.connect(neurons[0], neurons[5], 'Proximal', 5)
cmg_synapse.connect(neurons[1], neurons[3], 'Proximal', 5)
cmg_synapse.connect(neurons[3], neurons[2], 'Proximal', 5)
cmg_synapse.connect(neurons[1], neurons[5], 'Proximal', 5)
cmg_synapse.connect(neurons[5], neurons[2], 'Proximal', 5)
cmg_synapse.connect(neurons[0], neurons[4], 'Proximal', 5)
cmg_synapse.connect(neurons[1], neurons[4], 'Proximal', 5)
cmg_synapse.connect(neurons[4], neurons[2], 'Proximal', 5)

fig = plt.figure()
ax = fig.add_subplot(111)

for i in range(1):
    
    npt = [0,0]
    
    for j in range(201):
        
        if (npt[0] == 1):
            
            if j == 17:
                
                neurons[0].force()
        else:
            
            if j == 1:
                
                neurons[0].force()
                
        if npt[1] == 1:
            
            if j == 17:
                
                neurons[1].force()
        else:
            
            if j == 1:
                
                neurons[1].force()
                    
        for k in range(6):
            
            neurons[k].tick(0)
        
        for k in range(6): 
            vols[k].append(neurons[k].v)


for i in range(6):
    
    ax.plot(vols[i], label = str(i))
ax.legend()

plt.show()            
        


