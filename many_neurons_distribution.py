'''
this is a suplementary code for introducing poisson based spike coding
'''
import cmg_neuron
import cmg_synapse
import matplotlib.pylab as plt
import numpy as np
import math
from matplotlib import cm

def poisson(lamb, k):
    return ( lamb**k * np.exp(-lamb)) / math.factorial(k)

n_neurons = 2

x = 0
y = 10
a = 0.02
b = 0.2
c = -65 
d = 8

neuron0 = cmg_neuron.Neuron(a,b,c,d,x,y,'Excitatory', 0)
x = 0.2
neuron1 = cmg_neuron.Neuron(a,b,c,d,x,y,'Excitatory', 1)
neuron0.stdp_allowed = True
neuron1.stdp_allowed = True

cmg_synapse.connect(neuron0, neuron1, 'Proximal', 3.45)
    
v0 = []
v1 = []

ws = []
last  = 0

fig = plt.figure()
ax0 = fig.add_subplot(211)
ax1 = fig.add_subplot(212)
prob = 0

for i in range(1000):
    
    prob += poisson(40, last)
    if np.random.random() < prob:
        neuron0.force()
        prob = 0
        last = -1
    last += 1
    
    neuron0.tick(0)
    neuron1.tick(0)
    
    ws.append(neuron0.post_synapses[0].weight)
    v0.append(neuron0.v)
    v1.append(neuron1.v)
    
    ax0.cla()
    ax1.cla()
    
    ax0.plot(ws, label = 'weight')
    ax1.plot(v0, label = 'v0')
    ax1.plot(v1, label = 'v1')

    ax0.legend()
    ax1.legend()
    plt.pause(0.01)

plt.show()
