###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 23.10.2020
###    


import cmg_neuron
import numpy as np

dt = 0.5             # Time step
threshold = 30       # Maximum membrane potential
speed_non = 0.15     # Non-myelinated transmission speed
speed_mye = 1.00     # Myelinated transmission speed
pro_dis = 'lol'
d_weight = None

class Synapse:
    def __init__(self, pre, post, pro_dis, weight):
        self.pre = pre
        self.post = post
        self.weight = weight
        self.t_arrive = 0
        self.signals = []
        self.pro_dis = pro_dis
        
        if pro_dis == 'Proximal':
            self.delay = np.linalg.norm(np.array([pre.x, pre.y]) - np.array([post.x, post.y])) / speed_non
            #print(self.delay)
        self.delay = 5 + np.random.random()*15
        
class Signal:
    def __init__(self):
        self.time = 0

def connect(pre, post, pro_dis, weight, delay):
    syn = Synapse(pre, post, pro_dis, weight)
    syn.delay = delay
    pre.post_synapses.append(syn)
    post.pre_synapses.append(syn)

def is_connected(a, b):
    for pos in a.post_synapses:
        if pos.pre == b or pos.post == b:
            return True

    for pos in a.pre_synapses:
        if pos.pre == b or pos.post == b:
            return True
    return False

