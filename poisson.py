###
#   this is an implementation of NEAT algorithm snn class
#   url = "git@gitlab.com:refe.fb/neat-algorithm.git"
#   author: abdurrezak efe (the Codefather) @ Computational Modelling Group UNAM
#   date : 03.11.2020
###  

import math
import numpy as np
import random
import matplotlib.pylab as plt

def poisson(lamb, k):
    return ( lamb**k * np.exp(-lamb)) / math.factorial(k)

ar = []
prev = 0
cnt = 0
k = 0

while cnt < 100:
    
    cnt += 1
    ar.append( poisson(20, k) + prev )
    k += 1
    
    if np.random.random() < poisson(20, k):
        k = 0
        prev = 0
    #else:
     #   prev = ar[-1]

plt.plot(ar)
plt.show()